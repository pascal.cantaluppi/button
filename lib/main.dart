// import 'package:flutter/material.dart';

// void main() {
//   runApp(new MaterialApp(home: new AwesomeButton()));
// }

// class AwesomeButton extends StatefulWidget {
//   @override
//   AwesomeButtonState createState() => new AwesomeButtonState();
// }

// class AwesomeButtonState extends State<AwesomeButton> {
//   var counter = 0;
//   var strings = ["Flutter", "is", "Awesome!"];
//   var currentString = "";
//   void onPressed() {
//     setState(() {
//       currentString = strings[counter];
//       counter = counter < 2 ? counter + 1 : 0;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return new Scaffold(
//         appBar: new AppBar(
//             title: new Text("Awesome Button App"),
//             backgroundColor: Colors.black),
//         body: new Container(
//             child: new Center(
//                 child: new Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             new Text(currentString, style: new TextStyle(
//               fontSize: 40,
//               fontWeight: FontWeight.bold
//             )),
//             new Padding(padding: new EdgeInsets.all(30.0)),
//             new ElevatedButton(onPressed: onPressed, child: new Text(" click me "),
//             style: ElevatedButton.styleFrom(
//                 primary: Colors.grey,
//                 padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
//                 textStyle: TextStyle(
//                 fontSize: 20,
//                 fontWeight: FontWeight.bold)),)
//           ],
//         ))));
//   }
// }

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Elevated Button with Icon Example',
      home: FlutterExample(),
    );
  }
}

class FlutterExample extends StatelessWidget {
  const FlutterExample() : super();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Elevated Button with Icon and Text')),
        body: Center(
            child: ElevatedButton.icon(
          icon: Icon(
            Icons.favorite,
            color: Colors.pink,
            size: 24.0,
          ),
          label: Text('Elevated Button'),
          onPressed: () {
            print('Pressed');
          },
          style: ElevatedButton.styleFrom(
            shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(20.0),
            ),
          ),
        )));
  }
}